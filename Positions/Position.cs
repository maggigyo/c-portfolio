﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb1
{
    public class Position
    {
        private int x;
        public int X
        {
            get { return x; }
            set
            {
                if (value < 0)
                { x = 0; } //No negative coordinates!
                else
                { x = value; }
            }

        }


        private int y;
        public int Y
        {
            get { return y; }
            set
            {
                if (value < 0)
                { y = 0; } //No negative coordinates!
                else
                { y = value; }
            }
        }

        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return "(" + this.X + "," + this.Y + ")";
        }


        ///<summary>Calculates the distance from origo</summary> 
        ///<returns>returns a double for the distance</returns>
        public double Length()
        {
            double distance = Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
            return distance;
        }

        /// <summary>
        /// Checks to see if two Positions are at the same location
        /// </summary>
        /// <param name="p">the Position to compare with</param>
        /// <returns>true if same, otherwise false</returns>
        public bool Equals(Position p)
        {
            if (p.X == this.X && p.Y == this.Y)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Creates a copy of the Position
        /// </summary>
        /// <returns>the Position copy</returns>
        public Position Clone()
        {
            return new Position(X, Y);
        }

        //-----------------------------Overloaded operators-------------------------
        /// <summary>
        /// Compares the Positions' distances from origo
        /// </summary>
        /// <param name="p1">Position</param>
        /// <param name="p2">Position</param>
        /// <returns>true or false</returns>
        public static bool operator >(Position p1, Position p2)
        {
            if (p1.Length() > p2.Length())
            {
                return true;
            }
            else if (p1.Length() < p2.Length())
            {
                return false;
            }
            else //their lengths are the same!
            {
                return (p1.X > p2.X); //Compare X instead!
                                      //If x is also equal, the comparision will result in false.
                                      //This is an edge case, so the error is not that important.
            }

        }

        /// <summary>
        /// Compares the Positions' distances from origo
        /// </summary>
        /// <param name="p1">Position</param>
        /// <param name="p2">Position</param>
        /// <returns>true or false</returns>
        public static bool operator <(Position p1, Position p2)
        {
            return !(p1 > p2); //Uses function above, returns opposite
        }


        /// <summary>
        /// Adds the coordinates of two Positions. Creates a new position with the sums.
        /// </summary>
        /// <param name="p1">Position</param>
        /// <param name="p2">Position</param>
        /// <returns>A new Position</returns>
        public static Position operator +(Position p1, Position p2)
        {
            return new Position((p1.X + p2.X), (p1.Y + p2.Y));
        }

        /// <summary>
        /// Subtracts the coordinates of two Positions. Creates a new position with the difference.
        /// NOTE: Coordinates will not be set to negative numbers, only zero.
        /// </summary>
        /// <param name="p1">Position</param>
        /// <param name="p2">Position</param>
        /// <returns>A new Position</returns>
        public static Position operator -(Position p1, Position p2)
        {
            return new Position((p1.X - p2.X), (p1.Y - p2.Y));
        }

        /// <summary>
        /// Calculates the distance between two positions.
        /// </summary>
        /// <param name="p1">Position</param>
        /// <param name="p2">Position</param>
        /// <returns>A double with the distance</returns>
        public static double operator %(Position p1, Position p2)
        {
            return Math.Sqrt( Math.Pow(p1.X - p2.X, 2) 
                             + Math.Pow(p1.Y - p2.Y, 2) );
        }

    }
}
