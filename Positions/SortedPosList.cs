﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb1
{
    public class SortedPosList
    {
        private List<Position> positions;
        private string filePath;
        private bool doneReading = true;


        public Position this[int index]
        {
            get { return positions[index]; }
            //No setter, to keep the lists order
        }


        public SortedPosList()
        {
            positions = new List<Position>();
        }

        /// <summary>
        /// Creates a new SortedPosList from the specified file.
        /// If the file doesn't exist, it will be created instead.
        /// Saves the filePath in a string, so the file can be synked later.
        /// </summary>
        /// <param name="filePath"></param>
        public SortedPosList(string filePath)
        {
            positions = new List<Position>();
            this.filePath = filePath;
            if (File.Exists(filePath))
            {
                doneReading = false; //We need to read Positions from the file and add them
                string[] lines = System.IO.File.ReadAllLines(filePath);
                foreach (string line in lines)
                {
                    Parse(line);
                }
            }
            else
            {
                File.Create(filePath).Close();
            }

            doneReading = true; //We are done taking info from file!            

        }

        /// <summary>
        /// Takes a string and extracts the values needed to create a new Position.
        /// Then adds that position to the positions list.
        /// </summary>
        /// <param name="line">the string that should be parsed</param>
        private void Parse(string line)
        {
            int split = line.IndexOf(',');
            string number1 = line.Substring(1, split - 1);
            string number2 = line.Substring(split + 1, (line.Length - split - 2));
            
            Position p = new Position(stringToInt(number1), stringToInt(number2));
            this.Add(p);
        }

        /// <summary>
        /// A custom parser for turning string to int
        /// </summary>
        /// <param name="number">the string containing the number</param>
        /// <returns>an int</returns>
        private int stringToInt(string number)
        {
            int numberInt = 0;
            int multi = (int)Math.Pow(10, (number.Length - 1));
            
            foreach (char c in number)
            {
                numberInt += int.Parse(c.ToString()) * multi;
                multi /= 10;
            }
            return numberInt;

        }




        /// <summary>
        /// Returns the number of Positions in SortedPosList's private list
        /// </summary>
        /// <returns>an int with the number of Positions</returns>
        public int Count()
        {
            return positions.Count;
        }


        /// <summary>
        /// Adds a Position to the list.
        /// Then sorts the list using OrderBy
        /// First sorts by length, then by X if length is equal
        /// </summary>
        /// <param name="pos">The position to be inserted</param>
        public void Add(Position pos)
        {
            int middle;
            int start = 0;
            int end = positions.Count() - 1;
            while (end > start)
            {
                middle = (start + end) / 2;
                if (positions[middle] > pos)
                {//We need to look at smaller Positions
                    end = middle - 1;
                }
                else if (positions[middle] < pos)
                {//We need to look at larger positions
                    start = middle + 1;
                }
                else// They are equal!
                {
                    positions.Insert(middle, pos);
                    break;
                }
            }
            if (positions.Count == 0 || pos < positions[start])//It's smaller than any current Pos.
            {
                positions.Insert(start, pos);
            }
            else if (pos > positions[end]) //It's larger than any current Pos.
            {
                positions.Add(pos);
            }


            if (doneReading == true) //We have already taken info from file
            {//So this must be new info, that should be put in the file
                System.IO.StreamWriter file = new System.IO.StreamWriter(filePath);
                {
                    foreach (Position posi in positions)
                    {
                        file.WriteLine(posi.ToString());
                    }
                }
                file.Close();
            }


            return;

        }

        /// <summary>
        /// Searches for an identical Position. Then removes that Position from the list.
        /// </summary>
        /// <param name="pos">The Position to be removed</param>
        /// <returns>true if removal was successful, otherwise false</returns>
        public bool Remove(Position pos)
        {
            //Returns the index of the first found occurence
            int index = positions.FindIndex(p => p.X == pos.X && p.Y == pos.Y);

            if (index == -1) { return false; } //Position was not found

            positions.RemoveAt(index);
            //Update file
            return true;
        }

        /// <summary>
        /// Removes the Position at the specified index.
        /// NOTE: Using the above 'Remove' method is only suitable if the index is not already known!
        /// </summary>
        /// <param name="index">an int with the index number</param>
        public void RemoveAt(int index)
        {
            positions.RemoveAt(index);
        }

        /// <summary>
        /// Creates a clone of the SortedPosList
        /// </summary>
        /// <returns>The new SortedPosList</returns>
        public SortedPosList Clone()
        {
            SortedPosList newList = new SortedPosList();
            foreach (Position pos in positions)
            {
                newList.Add(pos.Clone());
            }

            return newList;
        }

        /// <summary>
        /// Creates a copy of a list, then removes the Positions not within the specified circle area.
        /// </summary>
        /// <param name="centerPos">A Position representing the middle point of the circle</param>
        /// <param name="radius">The radius of the circle</param>
        /// <returns>A SortedPosList with only Positions within the circle</returns>
        public SortedPosList circleContent(Position centerPos, double radius)
        {

            SortedPosList newList = this.Clone();
            int count = newList.Count();
            int i = 0;
            while (i < count)
            {

                if (newList[i] % centerPos > radius) //If the distance between the Positions is more than radius
                {
                    newList.RemoveAt(i);//Remove the element
                    count--; //One less element to look through

                }
                else { i++; }// Not removed, move to next
            }

            return newList;
        }

        //Save
        //Updates data on file

        //Load
        //Loads data from file

        //-------------------------------Overloaded operators--------------------------------------


        /// <summary>
        /// Creates a combined list containing the Positions from both lists, 
        /// sorted by Length (X value is secondary sort).
        /// </summary>
        /// <param name="sp1">A SortedPosList</param>
        /// <param name="sp2">A SortedPosList</param>
        /// <returns>The combined SortedPosList</returns>
        public static SortedPosList operator +(SortedPosList sp1, SortedPosList sp2)
        {
            SortedPosList joinedList = sp1.Clone();
            foreach (Position p in sp2.positions)
            {
                joinedList.Add(p);
            }

            return joinedList;
        }


        /// <summary>
        /// Creates a new SortedPosList that contains only Positions unique to sp1.
        /// (I.E It removes the Positions that exist in both sp1 and sp2 from the cloned list)
        /// </summary>
        /// <param name="sp1">The SortedPosList that should be cloned</param>
        /// <param name="sp2">The SortedPosList that will be compared with</param>
        /// <returns>A new SortedPosList</returns>
        public static SortedPosList operator -(SortedPosList sp1, SortedPosList sp2)
        {
            SortedPosList diffList = sp1.Clone();
            int dIndex = 0; //Index of diffList
            int sIndex = 0; //Index of sp2
            while (dIndex < diffList.Count() && sIndex < sp2.Count())
            {
                if (diffList[dIndex].Equals(sp2[sIndex]))
                {
                    diffList.RemoveAt(dIndex);
                }
                else if (diffList[dIndex].Length() < sp2[sIndex].Length())
                {
                    dIndex++;//No smaller Positions exist, so look at next in list!
                }
                else //diffList Pos. is larger, or its X is larger. (Length might still be same.)
                {
                    sIndex++;//We need to compare with larger Positions
                }
            }//We've reached the end of one of the lists!

            return diffList;
        }
    }
}
