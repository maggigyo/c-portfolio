﻿using System;
using SQLite;

namespace Labb2.Droid
{
	public class Account
	{
		[PrimaryKey]
		public string Number { get; set; }
		public string Name { get; set; }
		public string Type { get; set; }


		public Account ()
		{
		}

		public override string ToString()
		{
			return Name + "(" + Number + ")";
		}
	}
}

