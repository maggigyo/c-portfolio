﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;

namespace Labb2.Droid
{
	public class BookkeeperManager
	{
		private SQLiteConnection db;
		private static BookkeeperManager instance;
		public static BookkeeperManager Instance
		{
			get 
			{
				if (instance == null) 
				{
					instance = new BookkeeperManager ();
				}
				return instance;
			}
		}

		private BookkeeperManager ()
		{
			string path = System.Environment.GetFolderPath
				(System.Environment.SpecialFolder.Personal);
			db = new SQLiteConnection (path + @"\MDatabase.db" );


			try 
			{
				db.Table<Account> ().Count ();
			}
			catch//Table is empty
			{//All tables are probably empty
				//Create tables
				db.CreateTable<Account>();
				db.CreateTable<Entry>();
				db.CreateTable<TaxRate>();

				//Add income accounts:
				Account a1 = new Account{ Name = "Sales", Number = "0100", Type = "Income" };
				db.Insert (a1);
				a1 = new Account{ Name = "Loot", Number = "0200", Type = "Income" };
				db.Insert (a1);
				a1 = new Account{ Name = "Bribes", Number = "6666", Type = "Income" };
				db.Insert (a1);
				a1 = new Account{ Name = "Swear Jar", Number = "0101", Type = "Income" };
				db.Insert (a1);
				//Add expense accounts:
				a1 = new Account{ Name = "Games", Number = "2000", Type = "Expense" };
				db.Insert (a1);
				a1 = new Account{ Name = "Outgoing Bribes", Number = "2100", Type = "Expense" };
				db.Insert (a1);
				a1 = new Account{ Name = "Assorted Drugs", Number = "2200", Type = "Expense" };
				db.Insert (a1);
				a1 = new Account{ Name = "Foods", Number = "2300", Type = "Expense" };
				db.Insert (a1);
				a1 = new Account{ Name = "Gambling", Number = "2400", Type = "Expense" };
				db.Insert (a1);
				a1 = new Account{ Name = "Pets", Number = "2500", Type = "Expense" };
				db.Insert (a1);
				a1 = new Account{ Name = "Knitting Materials", Number = "2600", Type = "Expense" };
				db.Insert (a1);
				//Add money accounts:
				a1 = new Account{ Name = "Savings", Number = "5600", Type = "Money" };
				db.Insert (a1);
				a1 = new Account{ Name = "My Lover's Account", Number = "5700", Type = "Money" };
				db.Insert (a1);
				a1 = new Account{ Name = "Stolen Account", Number = "5800", Type = "Money" };
				db.Insert (a1);
				a1 = new Account{ Name = "Inheritance", Number = "5900", Type = "Money" };
				db.Insert (a1);
				a1 = new Account{ Name = "My Kids' College Funds", Number = "6000", Type = "Money" };
				db.Insert (a1);

				//Add tax rates
				TaxRate t1 = new TaxRate{ Rate = 0.5 };
				db.Insert (t1);
				t1 = new TaxRate{ Rate = 0.25 };
				db.Insert (t1);
				t1 = new TaxRate{ Rate = 0.1 };
				db.Insert (t1);
			}
				
		}

		public void AddEntry (Entry e)
		{
			db.Insert (e);
		}

		public List<Account> getAllIncomeAccounts ()
		{
			return db.Table<Account>().Where (a => a.Type == "Income").ToList();
		}

		public List<Account> getAllExpenseAccounts ()
		{
			return db.Table<Account>().Where (a => a.Type == "Expense").ToList();
		}

		public List<Account> getAllMoneyAccounts ()
		{
			return db.Table<Account>().Where (a => a.Type == "Money").ToList();
		}

		public List<TaxRate> getAllTaxRates ()
		{
			return db.Table<TaxRate> ().ToList ();
		}

		public List<Entry> getAllEntries ()
		{
			return db.Table<Entry> ().ToList ();
		}
	}
}

