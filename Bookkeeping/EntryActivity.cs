﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Labb2.Droid
{
	[Activity (Label = "Ny händelse")]			
	public class EntryActivity : Activity
	{
		private RadioButton rIncome;
		private RadioButton rExpense;
		private Spinner typeSpinner;
		private Spinner moneyAccountSpinner;
		private Spinner taxSpinner;
		private BookkeeperManager bm;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.Entry);

			bm = BookkeeperManager.Instance;

			Button createButton = FindViewById<Button> (Resource.Id.entry_button_add_entry);
			createButton.Click += addEntry;

			//The list for typespinner will be added later, in setTypeAccountSpinner
			typeSpinner = FindViewById<Spinner> (Resource.Id.entry_type_spinner);

			//Adds the list of tax rates to its spinner
			taxSpinner = FindViewById<Spinner> (Resource.Id.entry_VAT_spinner);
			ArrayAdapter <TaxRate> taxAdapter =
				new ArrayAdapter <TaxRate>(this, Android.Resource.Layout.SimpleSpinnerItem,
					bm.getAllTaxRates ());
			taxAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			taxSpinner.Adapter = taxAdapter;


			//Adds the list of money accounts to its spinner
			moneyAccountSpinner = FindViewById<Spinner> (Resource.Id.entry_account_spinner);
			ArrayAdapter <Account>moneyAdapter =
				new ArrayAdapter<Account>(this, Android.Resource.Layout.SimpleSpinnerItem,
				bm.getAllMoneyAccounts());
			moneyAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			moneyAccountSpinner.Adapter = moneyAdapter;

			//Functionality for the radio buttons
			rIncome = FindViewById<RadioButton> (Resource.Id.entry_radioButton_Inc);
			rExpense = FindViewById<RadioButton> (Resource.Id.entry_radioButton_Exp);
			rIncome.Click += setTypeAccountSpinner;
			rExpense.Click += setTypeAccountSpinner;
		


		}
			

		private void setTypeAccountSpinner (object sender, EventArgs e)
		{
			List<Account> typeAccounts;
			if (rIncome.Checked)//The type income has been chosen
			{
				typeAccounts = bm.getAllIncomeAccounts ();
			}
			else//The type expense has been chosen
			{
				typeAccounts = bm.getAllExpenseAccounts ();

			}

			ArrayAdapter <Account> typeAccountAdapter =
				new ArrayAdapter <Account>(this, Android.Resource.Layout.SimpleSpinnerItem, typeAccounts);
		
			typeAccountAdapter.SetDropDownViewResource 
				(Android.Resource.Layout.SimpleSpinnerDropDownItem);

			typeSpinner.Adapter = typeAccountAdapter;
		}

		private void addEntry(object sender, EventArgs e)
		{
			Entry newEntry = new Entry ();
			newEntry.Amount = Double.Parse(FindViewById<TextView>(Resource.Id.entry_amount).Text);
			newEntry.Date = FindViewById<TextView>(Resource.Id.entry_date).Text; 
			newEntry.Description = FindViewById<TextView>(Resource.Id.entry_description).Text;
			newEntry.Income = rIncome.Checked;

			newEntry.MoneyAccountId = ((ArrayAdapter<Account>)moneyAccountSpinner.Adapter).GetItem
				(moneyAccountSpinner.SelectedItemPosition).Number;

			newEntry.TypeAccountId = ((ArrayAdapter<Account>)typeSpinner.Adapter).GetItem
				(typeSpinner.SelectedItemPosition).Number;

			newEntry.TaxRateId = ((ArrayAdapter<TaxRate>)taxSpinner.Adapter).GetItem
				(taxSpinner.SelectedItemPosition).Id;
				
			bm.AddEntry (newEntry);

			Intent intent = new Intent (this, typeof(MainActivity));
			StartActivity (intent);
		}

	}
}

