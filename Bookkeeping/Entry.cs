﻿using System;
using SQLite;

namespace Labb2.Droid
{
	public class Entry
	{
		[PrimaryKey, AutoIncrement]
		public int Id{ get; private set;}
		public bool Income{ get; set; }
		public string Date{ get; set; }
		public string Description { get; set; }
		public string TypeAccountId { get; set; }
		public string MoneyAccountId { get; set; }
		public double Amount { get; set; }
		public int TaxRateId { get; set; }

		public Entry ()
		{
		}
	}
}

