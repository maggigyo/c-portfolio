﻿using System;
using SQLite;

namespace Labb2.Droid
{
	public class TaxRate
	{
		[PrimaryKey, AutoIncrement]
		public int Id{ get; private set;}
		public double Rate { get; set; }

		public TaxRate ()
		{
		}

		public override string ToString ()
		{
			return Rate.ToString ();
		}
	}
}

